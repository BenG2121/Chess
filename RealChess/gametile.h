#ifndef GAMETILE_H
#define GAMETILE_H



#include "globals.h"





extern int bBoardState[64];


class GameTile : public QWidget
{
    Q_OBJECT
public:
    explicit GameTile(QWidget *parent = 0);
    enChipType chipType;
    enPlayer playerNumber;
    unsigned int initialSquare;
    unsigned int initialBoardOffset_x;
    unsigned int initialBoardOffset_y;
    unsigned int currentSquare;
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
   // void mousePressEvent(QMouseEvent *event);
    void checkPossibleMoves(int);
    QGraphicsRectItem*  CreateTile(void);

public slots:
    void getTilePos(void);
};


#endif // GAMETILE_H
