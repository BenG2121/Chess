#ifndef KINGTILE_H
#define KINGTILE_H
#include "gametile.h"

class KingTile: public GameTile
{
public:
    KingTile();
    void DrawBrush(int);
    char help[40];
    void UpdateTilePosition(int);
};

#endif // KINGTILE_H
