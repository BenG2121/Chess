#-------------------------------------------------
#
# Project created by QtCreator 2016-05-23T21:23:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RealChess
TEMPLATE = app


SOURCES += main.cpp\
        main.cpp \
    board.cpp \
    chip.cpp \
    globals.cpp \
    gametile.cpp \
    kingtile.cpp

HEADERS  += \
    board.h \
    chip.h \
    globals.h \
    gametile.h \
    kingtile.h

FORMS    +=
