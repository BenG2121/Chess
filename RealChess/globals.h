#ifndef GLOBALS_H
#define GLOBALS_H


#define SQUAREWIDTH 74
#define BOARDSIZE 600
#define BoardOffset 7

#include <QMouseEvent>
#include <QKeyEvent>
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QBrush>
#include <QPixmap>
#include <QDebug>
#include <QGraphicsRectItem>
#include <QObject>
#include <QWidget>
#include <QTimer>

enum enChipType{
    chEmpty = 0,
    w_chBauer = 11,
    w_chBishop = 12,
    w_chTower = 13,
    w_chHorse = 14,
    w_chQueen = 15,
    w_chKing = 16,
    b_chBauer = 21,
    b_chBishop = 22,
    b_chTower = 23,
    b_chHorse = 24,
    b_chQueen = 25,
    b_chKing = 62
};
enum enPlayer{
    playerWhite = 1000,
    playerBlack = 2000
};





#endif // GLOBALS_H
