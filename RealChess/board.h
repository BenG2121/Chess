#ifndef BOARD_H
#define BOARD_H

#include "globals.h"
#include "chip.h"

extern int bBoardState[64];


class Board: public QWidget
{
    Q_OBJECT
public:
    explicit Board(QWidget *parent = 0);
    void initializeBoard(void);
    void moveChips();
    void keyPressEvent(QKeyEvent *event);
    QGraphicsRectItem*  CreateBoard(void);





    void mousePressEvent(QMouseEvent *event);
};

#endif // BOARD_H
