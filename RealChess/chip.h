#ifndef CHIP_H
#define CHIP_H

#include "globals.h"





extern int bBoardState[64];


class Chip: public QGraphicsRectItem, public QObject
{

public:
    Chip();
    enChipType chipType;
    enPlayer playerNumber;
    int currentSquare;
    int initialBoardOffset_x;
    void checkPossibleMoves(int);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mouseReleaseEvent(QMouseEvent *mevent);


    QGraphicsRectItem *helpRectColoredL = new QGraphicsRectItem();
    QGraphicsRectItem *helpRectColoredR = new QGraphicsRectItem();
    QGraphicsRectItem *helpRectColoredU = new QGraphicsRectItem();
    QGraphicsRectItem *helpRectColoredB = new QGraphicsRectItem();
    QGraphicsRectItem *helpRectColoredUL = new QGraphicsRectItem();
    QGraphicsRectItem *helpRectColoredUR = new QGraphicsRectItem();
    QGraphicsRectItem *helpRectColoredBL = new QGraphicsRectItem();
    QGraphicsRectItem *helpRectColoredBR = new QGraphicsRectItem();

};
#endif // CHIP_H
