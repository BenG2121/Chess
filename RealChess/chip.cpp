#include "chip.h"




Chip::Chip(){

    setRect(0,0,65,65);
    setBrush(Qt::red);
    setFlags(ItemIsFocusable|ItemIsSelectable|ItemIsMovable);

}

void Chip::checkPossibleMoves(int currentPos)
{
//    Chip::helpRectColoredR->setRect(x()+SQUAREWIDTH,y(),65,65);
//    Chip::helpRectColoredR->setBrush(Qt::green);
//    scene()->addItem(helpRectColoredR);

//    Chip::helpRectColoredL->setRect(x()-SQUAREWIDTH,y(),65,65);
//    Chip::helpRectColoredL->setBrush(Qt::green);
//    scene()->addItem(helpRectColoredL);

//    Chip::helpRectColoredU->setRect(x(),y()-SQUAREWIDTH,65,65);
//    Chip::helpRectColoredU->setBrush(Qt::green);
//    scene()->addItem(helpRectColoredU);

//    Chip::helpRectColoredB->setRect(x(),y()+SQUAREWIDTH,65,65);
//    Chip::helpRectColoredB->setBrush(Qt::green);
//    scene()->addItem(helpRectColoredB);

//    Chip::helpRectColoredUL->setRect(x()-SQUAREWIDTH,y()-SQUAREWIDTH,65,65);
//    Chip::helpRectColoredUL->setBrush(Qt::green);
//    scene()->addItem(helpRectColoredUL);

//    Chip::helpRectColoredUR->setRect(x()+SQUAREWIDTH,y()-SQUAREWIDTH,65,65);
//    Chip::helpRectColoredUR->setBrush(Qt::green);
//    scene()->addItem(helpRectColoredUR);

//    Chip::helpRectColoredBL->setRect(x()-SQUAREWIDTH,y()+SQUAREWIDTH,65,65);
//    Chip::helpRectColoredBL->setBrush(Qt::green);
//    scene()->addItem(helpRectColoredBL);

//    Chip::helpRectColoredBR->setRect(x()+SQUAREWIDTH,y()+SQUAREWIDTH,65,65);
//    Chip::helpRectColoredBR->setBrush(Qt::green);
//    scene()->addItem(helpRectColoredBR);


    /* WHITE CHIPS*/
    if(bBoardState[currentPos] == w_chBauer){
        qDebug() << "Before:" <<currentPos;
        if(currentPos>55){
            qDebug() << "End of Board";
        }
        if(bBoardState[currentPos+8] == 0)
        {
            qDebug() << "Straight possible";
            Chip::helpRectColoredB->setRect(x(),y()+SQUAREWIDTH,65,65);
            Chip::helpRectColoredB->setBrush(Qt::green);
            scene()->addItem(helpRectColoredB);
        }
        if((bBoardState[currentPos+7] >= b_chBauer) ){
            qDebug() << "Kill left possible";
            Chip::helpRectColoredBL->setRect(x()-SQUAREWIDTH,y()+SQUAREWIDTH,65,65);
            Chip::helpRectColoredBL->setBrush(Qt::green);
            scene()->addItem(helpRectColoredBL);
        }
        if((bBoardState[currentPos+9] >= b_chBauer) ){
            qDebug() << "Kill right possible";
            Chip::helpRectColoredBR->setRect(x()+SQUAREWIDTH,y()+SQUAREWIDTH,65,65);
            Chip::helpRectColoredBR->setBrush(Qt::green);
            scene()->addItem(helpRectColoredBR);
        }
    }else if(bBoardState[currentPos] == b_chBauer){
        if(currentPos < 8){
            qDebug() << "End of Board";
        }
        if(bBoardState[currentPos-8] == 0)
        {
            qDebug() << "Straight possible";
            Chip::helpRectColoredB->setRect(x(),y()-SQUAREWIDTH,65,65);
            Chip::helpRectColoredB->setBrush(Qt::green);
            scene()->addItem(helpRectColoredB);
        }
        if((bBoardState[currentPos-9] < b_chBauer) && bBoardState[currentPos-9] !=0 ){
            qDebug() << "Kill left possible";
            Chip::helpRectColoredBL->setRect(x()-SQUAREWIDTH,y()-SQUAREWIDTH,65,65);
            Chip::helpRectColoredBL->setBrush(Qt::green);
            scene()->addItem(helpRectColoredBL);
        }
        if((bBoardState[currentPos-7] < b_chBauer) && bBoardState[currentPos-7] !=0 ){
            qDebug() << "Kill right possible";
            Chip::helpRectColoredBR->setRect(x()+SQUAREWIDTH,y()-SQUAREWIDTH,65,65);
            Chip::helpRectColoredBR->setBrush(Qt::green);
            scene()->addItem(helpRectColoredBR);
        }
    }

    else{
        qDebug() << "no Bauer";
    }
    if(bBoardState[currentPos] == w_chKing)
    {


        if(currentPos<8){
            qDebug() << "King is at" << currentPos;
            qDebug() << currentPos-1;
            qDebug() << currentPos+1;
            qDebug() << currentPos+7;
            qDebug() << currentPos+8;
            qDebug() << currentPos+9;
        }
    }

}


void Chip::keyPressEvent(QKeyEvent *event)
{
    if((event->key() == Qt::Key_Space) & (event->modifiers() != Qt::CTRL) & (event->modifiers() != Qt::SHIFT)) {
        /* BAUERN CHIPS --------------------------------------------------------------------------------------------------------- */
        if(chipType == w_chBauer){
            if(bBoardState[currentSquare+8] == 0 && currentSquare < 56){
                bBoardState[currentSquare] = 0;
                bBoardState[currentSquare+8] = chipType;
                currentSquare = currentSquare+8;

                setPos(x(), y()+ SQUAREWIDTH);
                qDebug() << "White Bauer: Free!";
            }else{
                qDebug() << "White Bauer: Do nothing, path blocked by" << bBoardState[currentSquare+8];
            }
        }else if(chipType == b_chBauer){
            if(bBoardState[currentSquare-8] == 0 && currentSquare > 7){
                bBoardState[currentSquare] = 0;
                bBoardState[currentSquare-8] = chipType;
                currentSquare = currentSquare-8;

                setPos(x(), y()-SQUAREWIDTH);
                qDebug() << "Black Bauer: Free!";
            }else{
                qDebug() << "Black Bauer: Do nothing, path blocked by" << bBoardState[currentSquare-8];
            }

        }

    }
    /* HIT RIGHT -----------------------------------------------------------------------------------------------------------------*/
    else if ((event->key() == Qt::Key_Space) & (event->modifiers() == Qt::SHIFT)){
        /* BAUERN CHIPS --------------------------------------------------------------------------------------------------------- */
        if(chipType == w_chBauer){
            if(bBoardState[currentSquare+9] >= b_chBauer && (x()+SQUAREWIDTH+2 < BOARDSIZE)){
                bBoardState[currentSquare] = 0;
                bBoardState[currentSquare+9] = chipType;
                currentSquare = currentSquare+9;
                setPos(x()+ SQUAREWIDTH, y()+ SQUAREWIDTH);
                qDebug() << "White Bauer: Hit: Right!";
            }else{
                qDebug() << "White Bauer: Do nothing, nothing to hit!";
            }
        }else if(chipType == b_chBauer){
            if(bBoardState[currentSquare-7] != 0 && bBoardState[currentSquare-7] < b_chBauer   && (x()+SQUAREWIDTH+2 < BOARDSIZE)){
                bBoardState[currentSquare] = 0;
                bBoardState[currentSquare-7] = chipType;
                currentSquare = currentSquare-7;
                setPos(x()+ SQUAREWIDTH, y()- SQUAREWIDTH);
                qDebug() << "Black Bauer: Free!";
            }else{
                qDebug() << "Black Bauer: Do nothing, path blocked by" << bBoardState[currentSquare-8];
            }

        }
    }
    /* HIT LEFT ------------------------------------------------------------------------------------------------------------------*/
    else if ((event->key() == Qt::Key_Space) & (event->modifiers() == Qt::CTRL)){
        /* BAUERN CHIPS --------------------------------------------------------------------------------------------------------- */
        if(chipType == w_chBauer){
            if(bBoardState[currentSquare+7] >= b_chBauer && (x()-SQUAREWIDTH)>0){
                bBoardState[currentSquare] = 0;
                bBoardState[currentSquare+7] = chipType;
                currentSquare = currentSquare+7;
                setPos(x()- SQUAREWIDTH, y()+ SQUAREWIDTH);
                qDebug() << "White Bauer: Hit: Left!";
            }else{
                qDebug() << "White Bauer: Do nothing, nothing to hit!";
            }
        }else if(chipType == b_chBauer){
            if(bBoardState[currentSquare-9] != 0 && bBoardState[currentSquare-9] < b_chBauer   && (x()-SQUAREWIDTH)>0){
                bBoardState[currentSquare] = 0;
                bBoardState[currentSquare-9] = chipType;
                currentSquare = currentSquare-9;
                setPos(x() - SQUAREWIDTH, y()- SQUAREWIDTH);
                qDebug() << "Black Bauer: Free!";
            }else{
                qDebug() << "Black Bauer: Do nothing, path blocked by" << bBoardState[currentSquare-8];
            }

        }
    }else if(event->key() == Qt::Key_T){
        qDebug() << "Key T pressed!";
        if((chipType == w_chKing) || (chipType == b_chKing)){
            qDebug() << "KING has spoken!";
        }
        else if((chipType == w_chQueen)||(chipType == b_chQueen)){
            qDebug() << "QUEEN rules!";
        }
        else if((chipType == w_chBauer) || (chipType == b_chBauer))
        {
            qDebug() << "Der Pöbel!";
        }

    }else if(event->key() == Qt::Key_H)
        {
            qDebug() << "Key H pressed! Calculate possible moves!";
            checkPossibleMoves(currentSquare);
        }

}
void Chip::keyReleaseEvent(QKeyEvent *event){
    if(event->key() == Qt::Key_H){
        qDebug() << "H released";
        if(chipType == w_chBauer)
        {
        scene()->removeItem(helpRectColoredR);
        scene()->removeItem(helpRectColoredL);
        scene()->removeItem(helpRectColoredU);
        scene()->removeItem(helpRectColoredB);
        scene()->removeItem(helpRectColoredUL);
        scene()->removeItem(helpRectColoredUR);
        scene()->removeItem(helpRectColoredBL);
        scene()->removeItem(helpRectColoredBR);
        }else if (chipType == b_chBauer)
        {
        scene()->removeItem(helpRectColoredR);
        scene()->removeItem(helpRectColoredL);
        scene()->removeItem(helpRectColoredU);
        scene()->removeItem(helpRectColoredB);
        scene()->removeItem(helpRectColoredUL);
        scene()->removeItem(helpRectColoredUR);
        scene()->removeItem(helpRectColoredBL);
        scene()->removeItem(helpRectColoredBR);
        }
    }
}

void Chip::mouseReleaseEvent(QMouseEvent *mevent)
{

    if(mevent->button() == Qt::LeftButton)
    {
        qDebug() << "Chip: Left Button";
    }
}








