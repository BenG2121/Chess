#include "gametile.h"

GameTile::GameTile(QWidget *parent) : QWidget(parent)
{


}

void GameTile::keyPressEvent(QKeyEvent *event)
{

}

void GameTile::keyReleaseEvent(QKeyEvent *event)
{

}




QGraphicsRectItem*  GameTile::CreateTile(void){

    QGraphicsRectItem *item = new QGraphicsRectItem;
    QPixmap *pixmap;
    QBrush *brush = new QBrush();
    QTimer *timer = new QTimer();
     timer->start(1000);
    connect(timer, SIGNAL(timeout()), this, SLOT(getTilePos()));



    if       (chipType == w_chKing){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_w_k.JPG");
    }else if (chipType == b_chKing){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_b_k.JPG");
    }else if (chipType == w_chBauer){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_w_b.JPG");
    }else if (chipType == b_chBauer){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_b_b.JPG");
    }else if (chipType == w_chBishop){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_w_l.JPG");
    }else if (chipType == b_chBishop){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_b_l.JPG");
    }else if (chipType == w_chHorse){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_w_h.JPG");
    }else if (chipType == b_chHorse){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_b_h.JPG");
    }else if (chipType == w_chTower){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_w_t.JPG");
    }else if (chipType == b_chTower){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_b_t.JPG");
    }else if (chipType == w_chQueen){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_w_q.JPG");
    }else if (chipType == b_chQueen){
        pixmap = new QPixmap("D:\\programmieren\\RealChess\\chip_b_q.JPG");
    }

    if(pixmap->isNull()) {
        item->setBrush(Qt::red);
    }
    else {
        brush->setTexture(*pixmap);
        item->setBrush(*pixmap);
    }
    item->setRect(0,0,65,65);
    item->setPos(x()+BoardOffset+initialBoardOffset_x*SQUAREWIDTH,y()+BoardOffset+initialBoardOffset_y*SQUAREWIDTH);
    item->setFlags(QGraphicsItem::ItemIsMovable);

    return item;
}
void GameTile::getTilePos(void){
      qDebug() << "X:" << pos().x() << "   "  << "Y:" << pos().y();
}
