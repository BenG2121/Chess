#include "board.h"
#include "chip.h"
#include "gametile.h"
#include "kingtile.h"
#include "globals.h"




int bBoardState[64] = {0};


void initBoard(QGraphicsScene *);


int main(int argc, char *argv[])
{
    extern int bBoardState[64];
    QApplication a(argc, argv);

    QGraphicsScene *scene = new QGraphicsScene();
    QGraphicsView *view = new QGraphicsView(scene);
    QBrush *viewBrush = new QBrush(Qt::black);
    Board *myBoard = new Board;
    myBoard->setGeometry(0,0,600,600);

    scene->setSceneRect(0,0,1024,678);


    // add a view
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->show();
    view->setFixedSize(1024,678);
    view->setBackgroundBrush(*viewBrush);

    // add board to view
    scene->addWidget(myBoard);
    scene->addItem(myBoard->CreateBoard());



    initBoard(scene);

    return a.exec();

}

void initBoard(QGraphicsScene *scene){
    /* WHITE KING */
    KingTile *w_king = new KingTile;
    w_king->chipType = w_chKing;
    w_king->initialBoardOffset_y = 0;
    scene->addItem(w_king->CreateTile());

    /* BLACK KING */
    KingTile *b_king = new KingTile;
    b_king->chipType = b_chKing;
    b_king->initialBoardOffset_y = 7;
    scene->addItem(b_king->GameTile::CreateTile());
}



