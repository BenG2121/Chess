#include "board.h"


Board::Board(QWidget *parent) : QWidget(parent)
{

}
void Board::mousePressEvent(QMouseEvent * event){
    if(event->button()==Qt::LeftButton){
        qDebug() << "X:" << event->pos().x() << "   "  << "Y:" << event->pos().y();
    }
}

void Board::initializeBoard()
{
/* WHITE CHIPS ---------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------*/
    QPixmap *w_chipPixmapTower = new QPixmap("D:\\programmieren\\RealChess\\chip_w_t.JPG");
    if(w_chipPixmapTower->isNull()) {
    }
    else {
            QBrush *brush = new QBrush(*w_chipPixmapTower);
            brush->setTexture(*w_chipPixmapTower);
            //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
    }

    QPixmap *w_chipPixmapHorse = new QPixmap("D:\\programmieren\\RealChess\\chip_w_h.JPG");
    if(w_chipPixmapHorse->isNull()) {
    }
    else {
            QBrush *brush = new QBrush(*w_chipPixmapHorse);
            brush->setTexture(*w_chipPixmapHorse);
            //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
    }

    QPixmap *w_chipPixmapBishop = new QPixmap("D:\\programmieren\\RealChess\\chip_w_l.JPG");
    if(w_chipPixmapBishop->isNull()) {
    }
    else {
            QBrush *brush = new QBrush(*w_chipPixmapBishop);
            brush->setTexture(*w_chipPixmapBishop);
            //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
    }

    QPixmap *w_chipPixmapQueen = new QPixmap("D:\\programmieren\\RealChess\\chip_w_q.JPG");
    if(w_chipPixmapQueen->isNull()) {
    }
    else {
            QBrush *brush = new QBrush(*w_chipPixmapQueen);
            brush->setTexture(*w_chipPixmapQueen);
            //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
    }

    QPixmap *w_chipPixmapKing = new QPixmap("D:\\programmieren\\RealChess\\chip_w_k.JPG");
    if(w_chipPixmapKing->isNull()) {
    }
    else {
            QBrush *brush = new QBrush(*w_chipPixmapKing);
            brush->setTexture(*w_chipPixmapKing);
            //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
    }

    QPixmap *w_chipPixmapBauer = new QPixmap("D:\\programmieren\\RealChess\\chip_w_b.JPG");
    if(w_chipPixmapBauer->isNull()) {
    }
    else {
            QBrush *brush = new QBrush(*w_chipPixmapBauer);
            brush->setTexture(*w_chipPixmapBauer);
            //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
    }

/* BLACK CHIPS -------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------*/
    QPixmap *b_chipPixmapTower = new QPixmap("D:\\programmieren\\RealChess\\chip_b_t.JPG");
        if(b_chipPixmapTower->isNull()) {
        }
        else {
                QBrush *brush = new QBrush(*b_chipPixmapTower);
                brush->setTexture(*b_chipPixmapTower);
                //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
        }

        QPixmap *b_chipPixmapHorse = new QPixmap("D:\\programmieren\\RealChess\\chip_b_h.JPG");
        if(b_chipPixmapHorse->isNull()) {
        }
        else {
                QBrush *brush = new QBrush(*b_chipPixmapHorse);
                brush->setTexture(*b_chipPixmapHorse);
                //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
        }

        QPixmap *b_chipPixmapBishop = new QPixmap("D:\\programmieren\\RealChess\\chip_b_l.JPG");
        if(b_chipPixmapBishop->isNull()) {
        }
        else {
                QBrush *brush = new QBrush(*b_chipPixmapBishop);
                brush->setTexture(*b_chipPixmapBishop);
                //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
        }

        QPixmap *b_chipPixmapQueen = new QPixmap("D:\\programmieren\\RealChess\\chip_b_q.JPG");
        if(b_chipPixmapQueen->isNull()) {
        }
        else {
                QBrush *brush = new QBrush(*b_chipPixmapQueen);
                brush->setTexture(*b_chipPixmapQueen);
                //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
        }

        QPixmap *b_chipPixmapKing = new QPixmap("D:\\programmieren\\RealChess\\chip_b_k.JPG");
        if(b_chipPixmapKing->isNull()) {
        }
        else {
                QBrush *brush = new QBrush(*b_chipPixmapKing);
                brush->setTexture(*b_chipPixmapKing);
                //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
        }

        QPixmap *b_chipPixmapBauer = new QPixmap("D:\\programmieren\\RealChess\\chip_b_b.JPG");
        if(b_chipPixmapBauer->isNull()) {
        }
        else {
                QBrush *brush = new QBrush(*b_chipPixmapBauer);
                brush->setTexture(*b_chipPixmapBauer);
                //qDebug() <<"Chip: Brush Image Available";         //check if  brush image is available
        }

///* WHITE CHIPS ---------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------*/
//    Chip *w_Chip_1 = new Chip();
//    w_Chip_1->chipType = w_chBauer;
//    w_Chip_1->playerNumber = playerWhite;
//    w_Chip_1->initialBoardOffset_x = 0;
//    w_Chip_1->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_1->initialBoardOffset_x , y()+BoardOffset+SQUAREWIDTH );
//    w_Chip_1->currentSquare = 8;
//    w_Chip_1->setBrush(*w_chipPixmapBauer);

//    bBoardState[w_Chip_1->currentSquare] = w_Chip_1->chipType;
//    scene()->addItem(w_Chip_1);

//    Chip *w_Chip_2 = new Chip();
//    w_Chip_2->initialBoardOffset_x = 1;
//    w_Chip_2->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_2->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH);
//    scene()->addItem(w_Chip_2);
//    w_Chip_2->currentSquare = 9;
//    w_Chip_2->chipType = w_chBauer;
//    w_Chip_2->playerNumber = playerWhite;
//    w_Chip_2->setBrush(*w_chipPixmapBauer);
//    bBoardState[w_Chip_2->currentSquare] = w_Chip_2->chipType;

//    Chip *w_Chip_3 = new Chip();
//    w_Chip_3->initialBoardOffset_x = 2;
//    w_Chip_3->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_3->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH);
//    scene()->addItem(w_Chip_3);
//    w_Chip_3->currentSquare = 10;
//    w_Chip_3->chipType = w_chBauer;
//    w_Chip_3->playerNumber = playerWhite;
//    w_Chip_3->setBrush(*w_chipPixmapBauer);
//    bBoardState[w_Chip_3->currentSquare] = w_Chip_3->chipType;

//    Chip *w_Chip_4 = new Chip();
//    w_Chip_4->initialBoardOffset_x = 3;
//    w_Chip_4->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_4->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH);
//    scene()->addItem(w_Chip_4);
//    w_Chip_4->currentSquare = 11;
//    w_Chip_4->chipType = w_chBauer;
//    w_Chip_4->playerNumber = playerWhite;
//    w_Chip_4->setBrush(*w_chipPixmapBauer);
//    bBoardState[w_Chip_4->currentSquare] = w_Chip_4->chipType;

//    Chip *w_Chip_5 = new Chip();
//    w_Chip_5->initialBoardOffset_x = 4;
//    w_Chip_5->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_5->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH);
//    scene()->addItem(w_Chip_5);
//    w_Chip_5->currentSquare = 12;
//    w_Chip_5->chipType = w_chBauer;
//    w_Chip_5->playerNumber = playerWhite;
//    w_Chip_5->setBrush(*w_chipPixmapBauer);
//    bBoardState[w_Chip_5->currentSquare] = w_Chip_5->chipType;

//    Chip *w_Chip_6 = new Chip();
//    w_Chip_6->initialBoardOffset_x = 5;
//    w_Chip_6->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_6->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH);
//    scene()->addItem(w_Chip_6);
//    w_Chip_6->currentSquare = 13;
//    w_Chip_6->chipType = w_chBauer;
//    w_Chip_6->playerNumber = playerWhite;
//    w_Chip_6->setBrush(*w_chipPixmapBauer);
//    bBoardState[w_Chip_6->currentSquare] = w_Chip_6->chipType;

//    Chip *w_Chip_7 = new Chip();
//    w_Chip_7->initialBoardOffset_x = 6;
//    w_Chip_7->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_7->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH);
//    scene()->addItem(w_Chip_7);
//    w_Chip_7->currentSquare = 14;
//    w_Chip_7->chipType = w_chBauer;
//    w_Chip_7->playerNumber = playerWhite;
//    w_Chip_7->setBrush(*w_chipPixmapBauer);
//    bBoardState[w_Chip_7->currentSquare] = w_Chip_7->chipType;

//    Chip *w_Chip_8 = new Chip();
//    w_Chip_8->initialBoardOffset_x = 7;
//    w_Chip_8->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_8->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH);
//    scene()->addItem(w_Chip_8);
//    w_Chip_8->currentSquare = 15;
//    w_Chip_8->chipType = w_chBauer;
//    w_Chip_8->playerNumber = playerWhite;
//    w_Chip_8->setBrush(*w_chipPixmapBauer);
//    bBoardState[w_Chip_8->currentSquare] = w_Chip_8->chipType;

//    Chip *w_Chip_Tower_1 = new Chip();
//    w_Chip_Tower_1->chipType = w_chTower;
//    w_Chip_Tower_1->playerNumber = playerWhite;
//    w_Chip_Tower_1->initialBoardOffset_x = 0;
//    w_Chip_Tower_1->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_Tower_1->initialBoardOffset_x,y()+BoardOffset);
//    scene()->addItem(w_Chip_Tower_1);
//    w_Chip_Tower_1->currentSquare = 0;
//    w_Chip_Tower_1->setBrush(*w_chipPixmapTower);
//    bBoardState[w_Chip_Tower_1->currentSquare] = w_Chip_Tower_1->chipType;

//    Chip *w_Chip_Horse_1 = new Chip();
//    w_Chip_Horse_1->chipType = w_chHorse;
//    w_Chip_Horse_1->playerNumber = playerWhite;
//    w_Chip_Horse_1->initialBoardOffset_x = 1;
//    w_Chip_Horse_1->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_Horse_1->initialBoardOffset_x,y()+BoardOffset);
//    scene()->addItem(w_Chip_Horse_1);
//    w_Chip_Horse_1->currentSquare = 1;
//    w_Chip_Horse_1->setBrush(*w_chipPixmapHorse);
//    bBoardState[w_Chip_Horse_1->currentSquare] = w_Chip_Horse_1->chipType;

//    Chip *w_Chip_Bishop_1 = new Chip();
//    w_Chip_Bishop_1->chipType = w_chBishop;
//    w_Chip_Bishop_1->playerNumber = playerWhite;
//    w_Chip_Bishop_1->initialBoardOffset_x = 2;
//    w_Chip_Bishop_1->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_Bishop_1->initialBoardOffset_x,y()+BoardOffset);
//    scene()->addItem(w_Chip_Bishop_1);
//    w_Chip_Bishop_1->currentSquare = 2;
//    w_Chip_Bishop_1->setBrush(*w_chipPixmapBishop);
//    bBoardState[w_Chip_Bishop_1->currentSquare] = w_Chip_Bishop_1->chipType;

//    Chip *w_Chip_Queen = new Chip();
//    w_Chip_Queen->chipType = w_chQueen;
//    w_Chip_Queen->playerNumber = playerWhite;
//    w_Chip_Queen->initialBoardOffset_x = 3;
//    w_Chip_Queen->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_Queen->initialBoardOffset_x,y()+BoardOffset);
//    scene()->addItem(w_Chip_Queen);
//    w_Chip_Queen->currentSquare = 3;
//    w_Chip_Queen->setBrush(*w_chipPixmapQueen);
//    bBoardState[w_Chip_Queen->currentSquare] = w_Chip_Queen->chipType;

//    Chip *w_Chip_King = new Chip();
//    w_Chip_King->chipType = w_chKing;
//    w_Chip_King->playerNumber = playerWhite;
//    w_Chip_King->initialBoardOffset_x = 4;
//    w_Chip_King->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_King->initialBoardOffset_x,y()+BoardOffset);
//    scene()->addItem(w_Chip_King);
//    w_Chip_King->currentSquare = 4;
//    w_Chip_King->setBrush(*w_chipPixmapKing);
//    bBoardState[w_Chip_King->currentSquare] = w_Chip_King->chipType;



//    Chip *w_Chip_Bishop_2 = new Chip();
//    w_Chip_Bishop_2->chipType = w_chBishop;
//    w_Chip_Bishop_2->playerNumber = playerWhite;
//    w_Chip_Bishop_2->initialBoardOffset_x = 5;
//    w_Chip_Bishop_2->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_Bishop_2->initialBoardOffset_x,y()+BoardOffset);
//    scene()->addItem(w_Chip_Bishop_2);
//    w_Chip_Bishop_2->currentSquare = 5;
//    w_Chip_Bishop_2->setBrush(*w_chipPixmapBishop);
//    bBoardState[w_Chip_Bishop_2->currentSquare] = w_Chip_Bishop_2->chipType;

//    Chip *w_Chip_Horse_2 = new Chip();
//    w_Chip_Horse_2->chipType = w_chHorse;
//    w_Chip_Horse_2->playerNumber = playerWhite;
//    w_Chip_Horse_2->initialBoardOffset_x = 6;
//    w_Chip_Horse_2->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_Horse_2->initialBoardOffset_x,y()+BoardOffset);
//    scene()->addItem(w_Chip_Horse_2);
//    w_Chip_Horse_2->currentSquare = 6;
//    w_Chip_Horse_2->setBrush(*w_chipPixmapHorse);
//    bBoardState[w_Chip_Horse_2->currentSquare] = w_Chip_Horse_2->chipType;

//    Chip *w_Chip_Tower_2 = new Chip();
//    w_Chip_Tower_2->chipType = w_chTower;
//    w_Chip_Tower_2->playerNumber = playerWhite;
//    w_Chip_Tower_2->initialBoardOffset_x = 7;
//    w_Chip_Tower_2->setPos( (x()+BoardOffset) + SQUAREWIDTH*w_Chip_Tower_2->initialBoardOffset_x,y()+BoardOffset);
//    scene()->addItem(w_Chip_Tower_2);
//    w_Chip_Tower_2->currentSquare = 7;
//    w_Chip_Tower_2->setBrush(*w_chipPixmapTower);
//    bBoardState[w_Chip_Tower_2->currentSquare] = w_Chip_Tower_2->chipType;





///* BLACK CHIPS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------*/
//    Chip *b_Chip_1 = new Chip();
//    b_Chip_1->chipType = b_chBauer;
//    b_Chip_1->playerNumber = playerBlack;
//    b_Chip_1->initialBoardOffset_x = 1-1;
//    b_Chip_1->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_1->initialBoardOffset_x , y()+BoardOffset+SQUAREWIDTH*6 );
//    b_Chip_1->currentSquare = 48;
//    b_Chip_1->setBrush(*b_chipPixmapBauer);
//    bBoardState[b_Chip_1->currentSquare] = b_Chip_1->chipType;
//    scene()->addItem(b_Chip_1);

//    Chip *b_Chip_2 = new Chip();
//    b_Chip_2->initialBoardOffset_x = 2-1;
//    b_Chip_2->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_2->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH*6);
//    scene()->addItem(b_Chip_2);
//    b_Chip_2->currentSquare = 49;
//    b_Chip_2->chipType = b_chBauer;
//    b_Chip_2->playerNumber = playerBlack;
//    b_Chip_2->setBrush(*b_chipPixmapBauer);
//    bBoardState[b_Chip_2->currentSquare] = b_Chip_2->chipType;

//    Chip *b_Chip_3 = new Chip();
//    b_Chip_3->initialBoardOffset_x = 3-1;
//    b_Chip_3->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_3->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH*6);
//    scene()->addItem(b_Chip_3);
//    b_Chip_3->currentSquare = 50;
//    b_Chip_3->chipType = b_chBauer;
//    b_Chip_3->playerNumber = playerBlack;
//    b_Chip_3->setBrush(*b_chipPixmapBauer);
//    bBoardState[b_Chip_3->currentSquare] = b_Chip_3->chipType;

//    Chip *b_Chip_4 = new Chip();
//    b_Chip_4->initialBoardOffset_x = 4-1;
//    b_Chip_4->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_4->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH*6);
//    scene()->addItem(b_Chip_4);
//    b_Chip_4->currentSquare = 51;
//    b_Chip_4->chipType = b_chBauer;
//    b_Chip_4->playerNumber = playerBlack;
//    b_Chip_4->setBrush(*b_chipPixmapBauer);
//    bBoardState[b_Chip_4->currentSquare] = b_Chip_4->chipType;

//    Chip *b_Chip_5 = new Chip();
//    b_Chip_5->initialBoardOffset_x = 5-1;
//    b_Chip_5->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_5->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH*6);
//    scene()->addItem(b_Chip_5);
//    b_Chip_5->currentSquare = 52;
//    b_Chip_5->chipType = b_chBauer;
//    b_Chip_5->playerNumber = playerBlack;
//    b_Chip_5->setBrush(*b_chipPixmapBauer);
//    bBoardState[b_Chip_5->currentSquare] = b_Chip_5->chipType;

//    Chip *b_Chip_6 = new Chip();
//    b_Chip_6->initialBoardOffset_x = 6-1;
//    b_Chip_6->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_6->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH*6);
//    scene()->addItem(b_Chip_6);
//    b_Chip_6->currentSquare = 53;
//    b_Chip_6->chipType = b_chBauer;
//    b_Chip_6->playerNumber = playerBlack;
//    b_Chip_6->setBrush(*b_chipPixmapBauer);
//    bBoardState[b_Chip_6->currentSquare] = b_Chip_6->chipType;

//    Chip *b_Chip_7 = new Chip();
//    b_Chip_7->initialBoardOffset_x = 7-1;
//    b_Chip_7->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_7->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH*6);
//    scene()->addItem(b_Chip_7);
//    b_Chip_7->currentSquare = 54;
//    b_Chip_7->chipType = b_chBauer;
//    b_Chip_7->playerNumber = playerBlack;
//    b_Chip_7->setBrush(*b_chipPixmapBauer);
//    bBoardState[b_Chip_7->currentSquare] = b_Chip_7->chipType;

//    Chip *b_Chip_8 = new Chip();
//    b_Chip_8->initialBoardOffset_x = 8-1;
//    b_Chip_8->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_8->initialBoardOffset_x,y()+BoardOffset+SQUAREWIDTH*6);
//    scene()->addItem(b_Chip_8);
//    b_Chip_8->currentSquare = 55;
//    b_Chip_8->chipType = b_chBauer;
//    b_Chip_8->playerNumber = playerBlack;
//    b_Chip_8->setBrush(*b_chipPixmapBauer);
//    bBoardState[b_Chip_8->currentSquare] = b_Chip_8->chipType;

//    Chip *b_Chip_Tower_1 = new Chip();
//    b_Chip_Tower_1->chipType = b_chTower;
//    b_Chip_Tower_1->playerNumber = playerBlack;
//    b_Chip_Tower_1->initialBoardOffset_x = 0;
//    b_Chip_Tower_1->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_Tower_1->initialBoardOffset_x,y()+BoardOffset+7*SQUAREWIDTH);
//    scene()->addItem(b_Chip_Tower_1);
//    b_Chip_Tower_1->currentSquare = 56;
//    b_Chip_Tower_1->setBrush(*b_chipPixmapTower);
//    bBoardState[b_Chip_Tower_1->currentSquare] = b_Chip_Tower_1->chipType;

//    Chip *b_Chip_Horse_1 = new Chip();
//    b_Chip_Horse_1->chipType = b_chHorse;
//    b_Chip_Horse_1->playerNumber = playerBlack;
//    b_Chip_Horse_1->initialBoardOffset_x = 1;
//    b_Chip_Horse_1->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_Horse_1->initialBoardOffset_x,y()+BoardOffset+7*SQUAREWIDTH);
//    scene()->addItem(b_Chip_Horse_1);
//    b_Chip_Horse_1->currentSquare = 57;
//    b_Chip_Horse_1->setBrush(*b_chipPixmapHorse);
//    bBoardState[b_Chip_Horse_1->currentSquare] = b_Chip_Horse_1->chipType;

//    Chip *b_Chip_Bishop_1 = new Chip();
//    b_Chip_Bishop_1->chipType = b_chBishop;
//    b_Chip_Bishop_1->playerNumber = playerBlack;
//    b_Chip_Bishop_1->initialBoardOffset_x = 2;
//    b_Chip_Bishop_1->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_Bishop_1->initialBoardOffset_x,y()+BoardOffset+7*SQUAREWIDTH);
//    scene()->addItem(b_Chip_Bishop_1);
//    b_Chip_Bishop_1->currentSquare = 58;
//    b_Chip_Bishop_1->setBrush(*b_chipPixmapBishop);
//    bBoardState[b_Chip_Bishop_1->currentSquare] = b_Chip_Bishop_1->chipType;

//    Chip *b_Chip_Queen = new Chip();
//    b_Chip_Queen->chipType = b_chQueen;
//    b_Chip_Queen->playerNumber = playerBlack;
//    b_Chip_Queen->initialBoardOffset_x = 3;
//    b_Chip_Queen->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_Queen->initialBoardOffset_x,y()+BoardOffset+7*SQUAREWIDTH);
//    scene()->addItem(b_Chip_Queen);
//    b_Chip_Queen->currentSquare = 59;
//    b_Chip_Queen->setBrush(*b_chipPixmapQueen);
//    bBoardState[b_Chip_Queen->currentSquare] = b_Chip_Queen->chipType;

//    Chip *b_Chip_King = new Chip();
//    b_Chip_King->chipType = b_chKing;
//    b_Chip_King->playerNumber = playerBlack;
//    b_Chip_King->initialBoardOffset_x = 4;
//    b_Chip_King->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_King->initialBoardOffset_x,y()+BoardOffset+7*SQUAREWIDTH);
//    scene()->addItem(b_Chip_King);
//    b_Chip_King->currentSquare = 60;
//    b_Chip_King->setBrush(*b_chipPixmapKing);
//    bBoardState[b_Chip_King->currentSquare] = b_Chip_King->chipType;



//    Chip *b_Chip_Bishop_2 = new Chip();
//    b_Chip_Bishop_2->chipType = b_chBishop;
//    b_Chip_Bishop_2->playerNumber = playerBlack;
//    b_Chip_Bishop_2->initialBoardOffset_x = 5;
//    b_Chip_Bishop_2->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_Bishop_2->initialBoardOffset_x,y()+BoardOffset+7*SQUAREWIDTH);
//    scene()->addItem(b_Chip_Bishop_2);
//    b_Chip_Bishop_2->currentSquare = 61;
//    b_Chip_Bishop_2->setBrush(*b_chipPixmapBishop);
//    bBoardState[b_Chip_Bishop_2->currentSquare] = b_Chip_Bishop_2->chipType;

//    Chip *b_Chip_Horse_2 = new Chip();
//    b_Chip_Horse_2->chipType = b_chHorse;
//    b_Chip_Horse_2->playerNumber = playerBlack;
//    b_Chip_Horse_2->initialBoardOffset_x = 6;
//    b_Chip_Horse_2->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_Horse_2->initialBoardOffset_x,y()+BoardOffset+7*SQUAREWIDTH);
//    scene()->addItem(b_Chip_Horse_2);
//    b_Chip_Horse_2->currentSquare = 62;
//    b_Chip_Horse_2->setBrush(*b_chipPixmapHorse);
//    bBoardState[b_Chip_Horse_2->currentSquare] = b_Chip_Horse_2->chipType;

//    Chip *b_Chip_Tower_2 = new Chip();
//    b_Chip_Tower_2->chipType = b_chTower;
//    b_Chip_Tower_2->playerNumber = playerBlack;
//    b_Chip_Tower_2->initialBoardOffset_x = 7;
//    b_Chip_Tower_2->setPos( (x()+BoardOffset) + SQUAREWIDTH*b_Chip_Tower_2->initialBoardOffset_x,y()+BoardOffset+7*SQUAREWIDTH);
//    scene()->addItem(b_Chip_Tower_2);
//    b_Chip_Tower_2->currentSquare = 63;
//    b_Chip_Tower_2->setBrush(*b_chipPixmapTower);
//    bBoardState[b_Chip_Tower_2->currentSquare] = b_Chip_Tower_2->chipType;




}
void Board::keyPressEvent(QKeyEvent * event){

    if(event->key() == Qt::Key_Q){
        qDebug() << "Actual Board State";
        for(int i=0; i<64; i=i+8){
            qDebug("%2.i %2.i %2.i %2.i %2.i %2.i %2.i %2.i", bBoardState[i],bBoardState[i+1],bBoardState[i+2],bBoardState[i+3],bBoardState[i+4],bBoardState[i+5],bBoardState[i+6],bBoardState[i+7]);
        }
        qDebug() << "";

    }
}

QGraphicsRectItem *Board::CreateBoard()
{
    QGraphicsRectItem *item = new QGraphicsRectItem;
    QPixmap *pixmap = new QPixmap("D:\\programmieren\\RealChess\\ChessBoard.PNG");
    QBrush *brush = new QBrush();

    if(pixmap->isNull()) {
        item->setBrush(Qt::green);
    }
    else {
        brush->setTexture(*pixmap);
        item->setBrush(*pixmap);
    }
    item->setRect(0,0,600,600);


    return item;

}







